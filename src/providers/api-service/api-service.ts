import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Storage } from "@ionic/storage";
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { LoadingController } from 'ionic-angular';
import { Observable } from "rxjs/Observable"

/*
  Generated class for the ApiServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ApiServiceProvider {
  output:any;
  status:boolean;
  error: any;
  
  endpoin: any = 'https://beta-api.prioritas-group.com/';
  // endpoin: any = 'https://beta-api.prioritas-group.com/';
  // endpoin: any = 'http://api.prioritas-group.pro/';
  token: any  = '';
  key: any    = '44414E4954414D50414E2D424F474F5232303138';

  DataPost: Observable<any>
  DataGet: Observable<any>

  constructor(
    public http: HttpClient,
    private transfer: FileTransfer,
    private loading: LoadingController,
    private storage: Storage
    ) {
      storage.get('token').then(token => {
        this.token = token
      })
  }

  post(route, data, callback) {
      this.storage.get('token').then(token => {
        const headers = new HttpHeaders()
        .set("X-Api-Key", "")
        .set('Authorization', (token != null ? token:''))
        this.http.post(this.endpoin+route, data, {headers: headers}).subscribe((data: any) => {
            callback(data);
        }, error => {
          console.log(error)
          callback(error)
        })
      })
  }
  Post(route, data) {
    return new Promise( (resolve, reject) => {
      this.storage.get('token').then(token => {
          const headers = new HttpHeaders()
          .set("X-Api-Key", this.key)
          .set('Authorization', (token != null ? token:''))
          this.http.post(this.endpoin+route, data, {headers: headers})
          .subscribe(
              (data: Observable<any>) => {
                this.DataPost = data;
                resolve(this.DataPost)
              },
              err => { 
                  reject(err);
              }
          );
      })
    })
  }
  
  get(route, callback) {
      this.storage.get('token').then(token => {
        const headers = new HttpHeaders()
        .set("X-Api-Key", this.key)
        .set("Authorization", token);
        this.http.get(this.endpoin+route, {headers: headers}).subscribe((data: any) => {
          callback(data)
        },
        error => {
          callback(error)
        }
        );
      })
    // });
  }

  promise;
  Get(route, external=false) {
    let promise = new Promise( (resolve, reject) => {
      this.storage.get('token').then(token => {
        const headers = new HttpHeaders()
        .set("X-Api-Key", this.key)
        .set('Authorization', (token != null ? token:''))
        
        let url = external ? route : this.endpoin+route

        this.http.get(url, (!external ? {headers: headers}:{}))
        .subscribe(
            (data: Observable<any>) => {
              this.DataGet = data;
              resolve(this.DataGet)
            },
            err => { 
                this.promise = err
                reject(err);
            }
        );
      })
    })
    return promise;
  }
  
  progress: any;
  Upload(route, path, type='', name='') {
    const fileTransfer: FileTransferObject = this.transfer.create();
   
    path = path.replace(/(.*)\?(.*)/, '$1')

    let promise = new Promise( (resolve, reject) => {
      this.storage.get('token').then(token => {
        

        fileTransfer.onProgress((e)=>
        {
          let prg = (e.lengthComputable) ?  Math.round(e.loaded / e.total * 100) : -1;  
          console.log("progress:" + prg);
          this.progress = prg;
        });
        let progrssLoad = this.progress
        let loading = this.loading.create({
          content: "Sedang di upload..</br> "+progrssLoad
        })

        loading.present();
        var fileName = String(new Date().getTime()+'-'+Math.floor(Math.random() * Math.floor(12345678)))+'.jpg';
        let options: FileUploadOptions = {
          fileKey: 'file',
          fileName: fileName,
          headers: {
            'X-Api-Key': this.key,
            'Authorization': token
          },
          params: {
            'type': type,
            'nama_personal': name
          }
       }
       fileTransfer.upload(path, this.endpoin+route, options)
        .then((data) => {
          loading.dismiss()
          resolve(data)
        }, (err) => {
          loading.dismiss()
          reject(err)
        })

      }) //End GetToken
    }) // End Promise

    return promise
  }
  
}
