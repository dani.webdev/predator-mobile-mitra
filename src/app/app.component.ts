import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Storage } from "@ionic/storage";
import { LoginPage } from "../pages/login/login";
import { TabsPage } from "../pages/tabs/tabs";
import { ApiServiceProvider } from "../providers/api-service/api-service";
import { File } from "@ionic-native/file";
import { IntroPage } from '../pages/intro/intro';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any;

  constructor(
    platform: Platform, 
    statusBar: StatusBar, 
    splashScreen: SplashScreen, 
    storage: Storage,
    file: File,
    api: ApiServiceProvider
    ) {
    splashScreen.show();
    platform.ready().then(() => {
      statusBar.styleDefault();
      statusBar.backgroundColorByHexString("#33000000");
      this.rootPage = IntroPage
      if(platform.is('android')){
        
        file.checkDir(file.externalRootDirectory, 'PrioritasGroup').then( _ => {

            splashScreen.hide();
            this.rootPage = IntroPage

          }).catch(_ => {

            file.createDir(file.externalRootDirectory, 'PrioritasGroup', false).then((val) => {
            splashScreen.hide();
            alert('Created')

          }).catch(e => {
            splashScreen.hide();
            this.rootPage = IntroPage
            console.warn("error is "+e);
          });
            
        })
      }
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
    });//End Platform
  }
}
