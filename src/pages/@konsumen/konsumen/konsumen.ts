import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { ApiServiceProvider } from "../../../providers/api-service/api-service";

import { KonsumenDetailPage } from "../konsumen-detail/konsumen-detail";
import { KonsumenAddPage } from "../konsumen-add/konsumen-add";
/**
 * Generated class for the KonsumenPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-konsumen',
  templateUrl: 'konsumen.html',
})
export class KonsumenPage {

  result:any = [];
  noResult: boolean = false;
  originResult;

  loading;

  constructor(
    public navCtrl: NavController, 
    public api: ApiServiceProvider,
    public loadingCtrl: LoadingController,
    public navParams: NavParams) {
  }

  ionViewDidLoad() {
    this.loading = this.loadingCtrl.create({
      content: "Mohon Tunggu.."
    });

    this.loading.present()
    
    this.api.Get('konsumen/nama?search=').then((res: any) => {
      if(res.status) {
        this.result       = res.data.result;
        this.originResult = this.result;
        this.loading.dismiss()
      } else {
        this.loading.dismiss()
      }
    }).catch((err) => {
      alert('Error: ' + JSON.stringify(err))
    })
  }

  onInput(event) {
    let value   = event.srcElement.value ? event.srcElement.value:'';
    this.result = this.originResult;
    
    let result  = this.result;
    let output  = [];
    result.forEach((item, index) => {
      let checkNama = item.nama_lengkap.toLowerCase().indexOf(value.toLowerCase())
      let checkPendamping = item.nama_pendamping ? item.nama_pendamping.toLowerCase().indexOf(value):-1
      if(checkNama != -1 || checkPendamping != -1) {
        output.push(item);
      }
    })
    this.noResult = false;
    if(!output.length) {
      this.noResult = true;
    }
    this.result = output
  }

  goDetail(id) {
    this.navCtrl.push(KonsumenDetailPage, {
      id_konsumen: id
    })
  }

  goAdd() {
    this.navCtrl.push(KonsumenAddPage)
  }

}
