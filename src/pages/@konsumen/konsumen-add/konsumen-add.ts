import { Component, ViewChild } from '@angular/core';
import { 
  IonicPage, 
  NavController, 
  NavParams, 
  LoadingController,
  AlertController, 
  ViewController, 
  Content, 
  normalizeURL } from 'ionic-angular';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ApiServiceProvider } from '../../../providers/api-service/api-service';
import { ThrowStmt } from '@angular/compiler';
import { SelectSearchableComponent } from 'ionic-select-searchable';
import { Geolocation } from "@ionic-native/geolocation";
import { Camera, CameraOptions } from '@ionic-native/camera';
import { KonsumenDetailPage } from '../konsumen-detail/konsumen-detail';

@IonicPage()
@Component({
  selector: 'page-konsumen-add',
  templateUrl: 'konsumen-add.html',
})
export class KonsumenAddPage {

  @ViewChild(Content) content: Content;

  pageCheckKtp   = true;
  page           = -1;
  pageFinish     = false;

  type_personal  = "Pemohon"
  myPhoto;
  myPhotoPreview
  myKtp
  myKtpPreview

  // form       = 
  // {
  //   personal: {
  //     per_no_ktp: {value: '', isValid: true, message: ''}, 
  //     per_nama_lengkap: {value: '', isValid: true, message: 'Ini Wajib disini'},
  //     per_nama_panggilan: {value: '', isValid: true, message: ''},
  //     per_tempat_lahir: {value: '', isValid: true, message: ''},
  //     per_tgl_lahir: {value: '', isValid: true, message: ''},
  //     per_jenis_kelamin_id: {value: '', isValid: true, message: ''},
  //     per_agama_id: {value: '', isValid: true, message: ''},
  //     per_kewarganegaraan_id: {value: '', isValid: true, message: ''},
  //     per_daerahasal_suku: {value: '', isValid: true, message: ''},
  //     per_golongan_darah_id: {value: '', isValid: true, message: ''},
  //     per_telepon_1: {value: '', isValid: true, message: ''},
  //     per_telepon_2: {value: '', isValid: true, message: ''},
  //     per_email_1: {value: '', isValid: true, message: ''},
  //     per_email_2: {value: '', isValid: true, message: ''}
  //   },
  //   alamat: {
  //     alm_alamat: {value: '', isValid: true, message: ''},
  //     alm_provinsi_id: {value: '', isValid: true, message: ''},
  //     alm_kota_id: {value: '', isValid: true, message: ''},
  //     alm_kecamatan_id: {value: '', isValid: true, message: ''},
  //     alm_detail_lokasi: {value: '', isValid: true, message: ''},
  //     alm_kelurahan_id: {value: '', isValid: true, message: ''},
  //     alm_koordinat_lokasi: {value: '', isValid: true, message: ''},
  //   },
  //   keluarga: {
  //     kel_status_perkawinan_id: {value: '', isValid: true, message: ''},
  //     kel_tgl_menikah: {value: '', isValid: true, message: ''},
  //     kel_tanggungan_keluarga: {value: '', isValid: true, message: ''},
  //     kel_anak_nomor: {value: '', isValid: true, message: ''},
  //     kel_nama_ibu_kandung: {value: '', isValid: true, message: ''}
  //   },
  //   pekerjaan: {
  //     ker_status_bekerja: {value: '', isValid: true, message: ''},
  //     ker_bidang_usaha_id: {value: '', isValid: true, message: ''},
  //     ker_jabatan_id: {value: '', isValid: true, message: ''},
  //     ker_nama_kantor: {value: '', isValid: true, message: ''},
  //     ker_penghasilan_id:{value: '', isValid: true, message: ''},
  //     ker_alamat_kantor: {value: '', isValid: true, message: ''},
  //     ker_telepon_kantor: {value: '', isValid: true, message: ''},
  //     ker_masa_kerja: {value: '', isValid: true, message: ''},      
  //   },
  //   photo: '',
  //   photo_ktp: '',
  //   type: ''
  // }

  formData = {
      per_no_ktp: {value: '', isValid: true, message: ''}, 
      per_nama_lengkap: {value: '', isValid: true, message: ''},
      per_nama_panggilan: {value: '', isValid: true, message: ''},
      per_tempat_lahir: {value: '', isValid: true, message: ''},
      per_tgl_lahir: {value: '', isValid: true, message: ''},
      per_jenis_kelamin_id: {value: '', isValid: true, message: ''},
      per_agama_id: {value: '', isValid: true, message: ''},
      per_kewarganegaraan_id: {value: '', isValid: true, message: ''},
      per_daerahasal_suku: {value: '', isValid: true, message: ''},
      per_golongan_darah_id: {value: '', isValid: true, message: ''},
      per_telepon_1: {value: '', isValid: true, message: ''},
      per_telepon_2: {value: '', isValid: true, message: ''},
      per_email_1: {value: '', isValid: true, message: ''},
      per_email_2: {value: '', isValid: true, message: ''},

      alm_alamat: {value: '', isValid: true, message: ''},
      alm_provinsi_id: {value: '', isValid: true, message: ''},
      alm_kota_id: {value: '', isValid: true, message: ''},
      alm_kecamatan_id: {value: '', isValid: true, message: ''},
      alm_detail_lokasi: {value: '', isValid: true, message: ''},
      alm_kelurahan_id: {value: '', isValid: true, message: ''},
      alm_koordinat_lokasi: {value: '', isValid: true, message: ''},


      kel_status_perkawinan_id: {value: '', isValid: true, message: ''},
      kel_tgl_menikah: {value: '', isValid: true, message: ''},
      kel_tanggungan_keluarga: {value: '', isValid: true, message: ''},
      kel_anak_nomor: {value: '', isValid: true, message: ''},
      kel_nama_ibu_kandung: {value: '', isValid: true, message: ''},


      ker_status_bekerja: {value: '', isValid: true, message: ''},
      ker_bidang_usaha_id: {value: '', isValid: true, message: ''},
      ker_jabatan_id: {value: '', isValid: true, message: ''},
      ker_nama_kantor: {value: '', isValid: true, message: ''},
      ker_penghasilan_id:{value: '', isValid: true, message: ''},
      ker_alamat_kantor: {value: '', isValid: true, message: ''},
      ker_telepon_kantor: {value: '', isValid: true, message: ''},
      ker_masa_kerja: {value: '', isValid: true, message: ''},
           
      pen_informasi_tingkat_pendidikan_id: {value: '', isValid: true, message: ''},
      pen_status_masih_pendidikan: {value: '', isValid: true, message: ''},
      pen_jurusan_studi: {value: '', isValid: true, message: ''},
      pen_nama_sekolah: {value: '', isValid: true, message: ''},

      photo: {value: 'photo.jpg', isValid: true, message: ''},
      photo_ktp: {value: 'photo.jpg', isValid: true, message: ''},
      type: {value: 'pemohon', isValid: true, message: ''}
    }

  regional = {
    provinsi: [],
    kota: [],
    kecamatan: [],
    kelurahan: []
  }

  pekerjaan = {
    bidang_usaha: [],
    jabatan: []
  }


  administratif;

  geolocationLock = false;


  constructor(
    public navCtrl: NavController,
    private camera: Camera,
    private api: ApiServiceProvider,
    private loading: LoadingController,
    private view: ViewController,
    private Alert: AlertController,
    private geolocation: Geolocation,  
    public navParams: NavParams) {

      let load = this.loading.create({
        content: "Mohon Tunggu.."
      })

      this.type_personal = navParams.get('type_personal') ? navParams.get('type_personal'):'Pemohon'
      load.present()

      this.api.Get('master/administratif/getData').then((res: any) => {

        load.dismiss()
        this.administratif = res.data

      }).catch(err => {
        load.dismiss()
        alert('Error : '+JSON.stringify(err.error))      
      })
  }

  ionViewDidLoad() {

    this.view.showBackButton(true); // BackButton

    let watch = this.geolocation.watchPosition().subscribe((data) => {
      this.formData.alm_koordinat_lokasi.value = data.coords.latitude+', '+data.coords.longitude
    });

    if(this.geolocationLock) {
      watch.unsubscribe();
    }

  }

  Provinsi() {
    let load = this.loading.create({
      content: "Mohon Tunggu.."
    })

    load.present()
    this.api.Get('master/region/provinsi').then((res: any) => {
      load.dismiss()
      this.regional.provinsi = res.data
    }).catch(err => {
      load.dismiss()
      alert('Error : ' + JSON.stringify(err.error))      
    })
    
  }

  Kota(id: any) {
    let load = this.loading.create({
      content: "Mohon Tunggu.."
    })
    load.present()
    this.api.Get('master/region/kota?id_provinsi='+id).then((res: any) => {
      load.dismiss()
      this.regional.kota = res.data
    }).catch(err => {
      load.dismiss()
      alert('Error : '+JSON.stringify(err.error))      
    })
  }

  Kecamatan(id: any) {
    let load = this.loading.create({
      content: "Mohon Tunggu.."
    })
    load.present()
    this.api.Get('master/region/kecamatan?id_kota='+id).then((res: any) => {
      load.dismiss()
      this.regional.kecamatan = res.data
    }).catch(err => {
      load.dismiss()
      alert('Error : '+JSON.stringify(err.error))      
    })
  }

  Kelurahan(id: any) {
    this.regional.kelurahan= []
    let load = this.loading.create({
      content: "Mohon Tunggu.."
    })
    load.present()
    this.api.Get('master/region/kelurahan?id_kecamatan='+id).then((res: any) => {
      load.dismiss()
      this.regional.kelurahan = res.data
      console.log(this.regional.kelurahan)
    }).catch(err => {
      load.dismiss()
      alert('Error : '+JSON.stringify(err.error))      
    })
  }

  provChange(event: {
    conponent: SelectSearchableComponent,
    value: any
  }) {
    this.regional.kecamatan = this.regional.kelurahan = this.regional.kota = []
    this.formData.alm_kota_id.value     = this.formData.alm_kecamatan_id.value = this.formData.alm_kelurahan_id.value = ''
    // this.formData.alm_provinsi_id.value = event.value.id
    this.Kota(event.value.id)
  }
  
  kotaChange(event: {
    conponent: SelectSearchableComponent,
    value: any
  }) {
    this.regional.kecamatan = this.regional.kelurahan= []
    this.formData.alm_kecamatan_id.value  = this.formData.alm_kelurahan_id.value = ''
    // this.formData.alm_kota_id.value       = event.value.id
    this.Kecamatan(event.value.id)
  }

  kecChange(event: {
    conponent: SelectSearchableComponent,
    value: any
  }) {
    this.formData.alm_kelurahan_id.value  = ''
    // this.formData.alm_kecamatan_id.value  = event.value.id
    this.Kelurahan(event.value.id)
  }

  kelChange(event: {
    conponent: SelectSearchableComponent,
    value: any
  }) {
    // this.formData.alm_kelurahan_id.value  = event.value.id
  }

  checkKtp() {
    let load = this.loading.create({
      content: "Mohon Tunggu.."
    })

    load.present()
    let form = new FormData();
    form.append('no_ktp', this.formData.per_no_ktp.value)
    this.api.Post('personal/cek_ktp', form).then((res: any) => {      
      if(res.status != 'error' && !res.status) {
        
        this.pageCheckKtp = false;
        this.page = this.page+1
        
        this.view.showBackButton(false)
      } else if(res.status == 'error') {
        alert(res.message)
      } else {
        // alert(res.message)
        let alert = this.Alert.create({
          title: "Data Personal Sudah Ada..!",
          buttons: [
            {
              text: "Lihat",
              cssClass: "btn-default",
              handler: () => {

              }
            },
            {
              text: 'Batal'
            }
          ]
        });

        alert.present()
      }
      load.dismiss()
      this.Provinsi()
    }).catch(err => {

    })
  }

  getPekerjaan(id='') {
    console.log(id);
    let route = 'master/pekerjaan/get?id_usaha='+id;
    this.api.Get(route).then((res: any) => {
      if(id) {
        this.pekerjaan.jabatan = res.data;
      } else {
        this.pekerjaan.bidang_usaha = res.data
      }
    })
  }

  //Navigate
  next() {
    this.page = this.page+1
    if(this.page == 3) {
      this.pageFinish = true;
    }

    if(this.page == 2) {
      this.getPekerjaan()
    }
    this.content.scrollToTop()
  }

  back() {
    if(this.page == 0) {
      this.pageCheckKtp = true
      this.view.showBackButton(true)
    }

    if(this.pageFinish) {
      this.pageFinish = false
    }
    this.page = this.page-1

    this.content.scrollToTop()
  }

  finish() {
    let form = this.formData
    var FromData = new FormData();
    
    //Build Form Data From Object
    for( let key in form ) {
      FromData.append(key, form[key].value)
    }

    let loading = this.loading.create({
      content: "Mohon Tunggu..."
    });
    let Alert = this.Alert.create({
      title: "Anda yakin, data sudah lengkap..?",
      buttons: [
        {
          text: 'Yakin',
          cssClass: 'btn-default',
          handler: () => {
            loading.present()
            // this.navCtrl.popToRoot()
            // this.navCtrl.push(KonsumenDetailPage, {
            //   from: 'konsuemen-add' //Personal Add
            // })
            this.api.Post('personal/add', FromData).then((respon: any) => {
              loading.dismiss()
              if(!respon.status) {
                this.page = 0;
                this.pageFinish = false;
                
                let errors = respon.error
                
                for(let key in this.formData) {
                  this.formData[key].isValid = true;
                }
                for(let error in errors) {
                  this.formData[error].isValid = false;
                  this.formData[error].message = errors[error];
                }

              } else { //Success save Personal

                this.navCtrl.popToRoot();
                this.navCtrl.push(KonsumenDetailPage, {
                  from: KonsumenAddPage,
                  type: "pemohon",
                  id_personal: respon.data.personal_id
                })

              }

            }).catch((err: any) => {
              alert('Error :'+ err)
            })
          }
        },
        {
          text: 'Batal',
          cssClass: '',
          handler: () => {

          }
        }
      ]
    })

    Alert.present();

  }

  lockGeo() {
    this.geolocationLock = true;
  }

  takePhoto(typePhoto: any): void {

    const options: CameraOptions = {
      quality: 80,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      saveToPhotoAlbum: true,
    }
    
    this.camera.getPicture(options).then((imageData) => {
        this.api.Upload('personal/upload/documents', normalizeURL( imageData ), typePhoto).then((data: any) => {
          if(typePhoto == 'ktp') {
            let response                  = JSON.parse(data.response);
            this.myKtpPreview             = response.file.link
            this.formData.photo_ktp.value = response.file.filename;
            alert('Berhasil Upload : '+response.file.filename)
          } else {
            let response                  = JSON.parse(data.response);
            this.myPhotoPreview           = response.file.link
            this.formData.photo.value     = response.file.filename
            alert('Berhasil Upload : ' + response.file.filename)
          }
        }).catch(err => {
          alert(err)
        })
    }, (err) => {
      alert('Error : Akses Tidak di izinkan.')
    });
  }

  browsePhoto(typePhoto: any): void {

    let cameraOptions: CameraOptions = {
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      destinationType: this.camera.DestinationType.FILE_URI,
      // destinationType: this.camera.DestinationType.DATA_URL,      
      quality: 80,
      encodingType: this.camera.EncodingType.JPEG,      
      mediaType: this.camera.MediaType.PICTURE,
      // correctOrientation: true
    }

    this.camera.getPicture(cameraOptions)
    .then(imageData => {
        this.api.Upload('personal/upload/documents', normalizeURL ( imageData ) , typePhoto).then((data: any) => {
          if(typePhoto == 'photo') {
            let response            = JSON.parse(data.response)
            this.myPhotoPreview     = response.file.link
            this.formData.photo.value         = response.file.filename
            alert('Berhasil di Upload : '+data.response)
          } else {
            let response            = JSON.parse(data.response)
            this.myKtpPreview       = response.file.link
            this.formData.photo_ktp.value     = response.file.filename
            alert('Berhasil di Upload : '+response.file.filename)
          }
        }).catch(err => {
          alert(err)
        })
      }, 
      err => {
        alert(err)
      });  
  }

}
