import { Component } from '@angular/core';
import {
  IonicPage, 
  NavController, 
  NavParams, 
  AlertController, 
  LoadingController, 
  ViewController,
  ModalController } from 'ionic-angular';
import { LaunchNavigator } from "@ionic-native/launch-navigator";
import { ApiServiceProvider } from "../../../providers/api-service/api-service";
import { DetailOrderPage } from "../../@pemesanan/detail-order/detail-order";
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-konsumen-detail',
  templateUrl: 'konsumen-detail.html',
})
export class KonsumenDetailPage {
  
  dataPemohon;
  dataPendamping;
  dataPersonal;
  dataPersonalAlamat;

  type_data: string = 'Pemohon';

  comeFrom: any;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private Alert: AlertController,
    private load: LoadingController,
    private storage: Storage,
    private modal: ModalController,
    private api: ApiServiceProvider,
    private navigation: LaunchNavigator
    ) {
      this.comeFrom   = navParams.get('from') ? navParams.get('from'):false;
      let type_from   = navParams.get('type')
      let id_personal = navParams.get('id_personal')
      if(type_from == 'pemohon') {
        this.storage.set('personal_pemohon', {
          id_personal: id_personal
        })
      }
      if(this.comeFrom) {
        this.api.Get('personal/detail?id='+id_personal).then((respon: any) => {
          
          if(type_from == 'pemohon') {
            this.dataPemohon    = respon.data.result
          } else {
            this.dataPendamping = respon.data.result
          }

        }).catch(err => {
          alert('Error'+JSON.stringify(err))
        })
      }
  }

  ionViewDidLoad() {
    let loading = this.load.create({
      content: 'Memuat Data Pemohon...'
    });

    if(!this.comeFrom) {
      loading.present()
      let id = this.navParams.get('id_konsumen');
      this.api.Get('konsumen/detail?id='+id).then((res: any) => {
        loading.dismiss()
        this.dataPemohon  = res.data.result[0]
        this.dataPersonal = this.dataPemohon;
        this.getAlamat()
        if(this.dataPemohon.konsumen.pendamping_personal_id) {
          this.loadPendamping(this.dataPemohon.konsumen.pendamping_personal_id)
        }
      }).catch(err => {

      })
    }
  }

  loadPendamping(id='') {
    let loading = this.load.create({
      content: 'Memuat Data Pendamping...'
    });
    loading.present()
    this.api.Get('personal/detail?id='+id).then((res: any) => {
      loading.dismiss()
      this.dataPendamping = res.data.result[0];
    }).catch(err => {

    })
  }
  getAlamat() {
    let alamat = this.dataPersonal ? this.dataPersonal.alamat[0].alamat+', '+this.dataPersonal.alamat[0].kelurahan+', '+this.dataPersonal.alamat[0].kecamatan+', '+this.dataPersonal.alamat[0].kota+' - '+this.dataPersonal.alamat[0].provinsi:''
    this.dataPersonalAlamat = alamat != ', , ,  - ' ? alamat:'-'
  }

  getUmur(tanggal) {
    let tahun = tanggal.split('-');
    let umur  = (new Date(Date.now()).getFullYear())-tahun[0]
    return umur
  }

  selectKonsumen() {
    let alert = this.Alert.create({
      title: 'Anda yakin...?',
      subTitle: 'Tambahkan konsumen ini ke map order anda.',
      buttons: [
        {
          text: 'Yakin',
          cssClass: 'btn-default',
          handler: () => {
            this.navCtrl.pop()
            this.navCtrl.pop()
            this.navCtrl.push(DetailOrderPage, {
              id_konsumen: this.dataPemohon.konsumen.id,
              namaPemohon: { nama: this.dataPemohon.konsumen.nama_lengkap, photo: this.dataPemohon.konsumen.photo },
              namaPendamping: { nama: this.dataPemohon.konsumen.nama_pendamping, photo: this.dataPemohon.konsumen.photo_pendamping }
            })
          }
        },
        {
          text: 'Batal',
          handler: () => {
            this.navCtrl.pop()
          }
        }
      ]
    });
    alert.present()
  }

  switchData(type: string) {
    if(type == 'Pemohon') {
      this.type_data = type;
      this.dataPersonal = this.dataPemohon
      this.getAlamat()
    } else {
      if(this.dataPendamping) {
        this.type_data = type;
        this.dataPersonal = this.dataPendamping
        this.getAlamat()
      } else {
        alert('Merupakan Pemohon Tunggal')
      }
    }
  }

  openMap(tag) {
    tag = tag.split(' ')
    this.navigation.navigate([parseInt(tag[0]),parseInt(tag[1])]).then(success => {

    }, error => {
      alert('Error'+JSON.stringify(error))
    })
  }

  openDetail() {
    this.modal.create(ModalDetailKonsumen, {
     dataPersonal: this.dataPersonal
    }).present()
  }

  batal() {
    this.navCtrl.popToRoot();
  }
}

@Component({
  selector: 'modal-detail-konsumen',
  templateUrl: 'child/modal-detail-konsumen.html'
})

export class ModalDetailKonsumen {

  data;
  segment = -1;
  type_personal;
  constructor(
    private params: NavParams,
    private view: ViewController
  ){
    this.data = params.get('dataPersonal')
    this.type_personal = this.data.konsumen ? 'Pemohon':'Pendamping'
  }

  close() {
    this.view.dismiss();
  }

  getSeg(set) {
    this.segment = set;
  }
  getUmur = (tanggal) => {
    let tahun = tanggal.split('-');
    let umur  = (new Date(Date.now()).getFullYear())-tahun[0]
    return umur
  }
  formatDate(date) {
    var monthNames = [
      "Januari", "Februari", "Maret",
      "April", "Mei", "Juni", "Juli",
      "Augustus", "September", "Oktober",
      "November", "Desember"
    ];
  
    var day = new Date(date).getDate();
    var monthIndex = new Date(date).getMonth();
    var year = new Date(date).getFullYear();
  
    return day + ' ' + monthNames[monthIndex] + ' ' + year;
  }
}
