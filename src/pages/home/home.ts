import { Component } from '@angular/core';
import { App, NavController, LoadingController, NavParams, Slide } from 'ionic-angular';
import { Storage } from "@ionic/storage";
import { ApiServiceProvider } from "../../providers/api-service/api-service";

import { CartPage } from "../cart/cart";
import { ListProdukPage } from "../@persediaan/list-produk/list-produk";

@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html'
})

export class MenuPage {
  
  homePage  = HomePage

  pages     = [{
    page: HomePage,
    title: 'E-Katalog'
  }]


  constructor(
    public navCtrl: NavController,
    public loadingCtrl: LoadingController,
    public params: NavParams
    ) {

  }

  openPage(page) {
    this.navCtrl.setRoot(page)
  }

}


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  // title: 'E-Katalog'
  kategori: boolean = true;
  merek: boolean    = false;

  // dataItem: Observable<any>;
  Kategori;
  Merek;
  imageSlider:any;
  status=0; //Status Init

  countCart = 0;
  // Slider      = [];
  constructor(
    public app: App, 
    public navCtrl: NavController, 
    public loadingCtrl: LoadingController,
    public api: ApiServiceProvider, 
    public params: NavParams, 
    public storage: Storage
    ) {
  }
  ionViewDidEnter() {

  }
  ionViewWillEnter() {
    this.status = 0
    this.initPage()
  }
  initPage() {
    var loading = this.loadingCtrl.create({
      content: 'Mohon Tunggu...<br><small>Orang sabar di sayang Tuhan :)<small>'
    })
    loading.present();
    this.status = 0;
    let totalInit = 2;
    if(!this.Kategori) {
      totalInit = totalInit+1;
      this.api.Get('barang/kategori').then((kategori: any) => {
        let respond = []
        kategori = kategori.data
        for(let index in kategori) {
          let count = kategori[index].nama_kategori.split(',')[1].replace(/\((.*)\)/,'$1');
          if( parseInt(count) )
          {
            respond.push({
              image: kategori[index].image,
              name: kategori[index].nama_kategori
            })
          }
        }
        this.Kategori = respond
        this.status = this.status+1
        if(this.status >= totalInit) {
          loading.dismiss()
        }
      }).catch(() => {
        loading.dismiss()
      })//End Get Kategori
    }
    this.api.Get('promosi/images/slider').then((slider: any) => {
      var imageSlider = slider.data;
      for(let index in imageSlider) {
        imageSlider[index] = {
          image: imageSlider[index].image,
          name: imageSlider[index].nama_kategori
        }
      }
      this.imageSlider = imageSlider
      this.status = this.status+1;
      if(this.status >= totalInit) {
        loading.dismiss()
      }
    }).catch(() => {
      loading.dismiss()
    }) //End Get Slider

    this.api.Get('pemesanan/cart/count').then((res: any) => {
      this.countCart = res;
      this.status = this.status+1;
      if(this.status >= totalInit) {
        loading.dismiss()
      }
    }).catch(() => {
        loading.dismiss()
        console.log('Error COunt')
    })
  }

  selectTab(type) {
    if(type == 'kategori') {
      this.kategori = true;
      this.merek    = false;
    }
    if(type == 'merek') {
      this.merek = true;
      this.kategori = false;
      if(!this.Merek) {
        var loading = this.loadingCtrl.create({
          content: 'Mohon Tunggu...<br><small>Orang sabar di sayang Tuhan :)<small>'
        })
        loading.present()

        this.api.Get('barang/merek').then((loadMerek: any) => {
          let loadData = []
          loadMerek = loadMerek.data
          for(let index in loadMerek) {
            loadData.push({
              image: loadMerek[index].image,
              name: loadMerek[index].nama
            })
          }
          this.Merek = loadData
          this.status = this.status+1
          loading.dismiss()
        }).catch((err) => {
          loading.dismiss()
        })//End Get Merek
      }
    }
  }

  getProduk(query='', type=''){
    var loading = this.loadingCtrl.create({
      content: 'Memuat Produk...<br><small>"Orang sabar di sayang Tuhan :)"<small>'
    })
    loading.present()

    let url = type == 'merek' ? 'merek='+query:'kategori='+query 
    this.api.Get('persediaan/filter?'+url).then((res:any) => {
       let outputProduk = res.data.result
        this.navCtrl.push(ListProdukPage,{
          produk: outputProduk
        })
      loading.dismiss()      
    }).catch(err => {
      alert("Kemungkinan tidak ada koneksi.\n"+err.name+err.statusText)
      loading.dismiss()
    })
  }

  goCart() {
    this.navCtrl.push(CartPage)
  }

  onEnterSearch(Element) {
    var value = Element.value
    var loading = this.loadingCtrl.create({
      content: 'Memuat Produk...<br><small>"Orang sabar di sayang Tuhan :)"<small>'
    })
    loading.present()
    this.api.get('persediaan/filter?nama='+value, result => {
      if(result.data.result) {
        let outputProduk = result.data.result
        this.navCtrl.push(ListProdukPage,{
          produk: outputProduk,
          fillSearch: Element.value
        })
      } else {

      }
      loading.dismiss()
    })
  }
  async doRefresh(refresher) {
    await this.initPage()
    refresher.complete();
  }
}
