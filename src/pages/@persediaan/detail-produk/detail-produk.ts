import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { Storage } from "@ionic/storage";
import { ApiServiceProvider } from '../../../providers/api-service/api-service';
import { TitleCasePipe } from '@angular/common';
import { CartPage } from "../../cart/cart";
import { KonsumenPage } from "../../@konsumen/konsumen/konsumen";
import { DetailOrderPage } from "../../@pemesanan/detail-order/detail-order";
/**
 * Generated class for the DetailProdukPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-detail-produk',
  templateUrl: 'detail-produk.html',
})
export class DetailProdukPage {

  imageSlider:any;
  detailProduk: any;

  data    = {
    downpayment:'',nama_barang:'',cicilan_bln:'',deskripsi:'',harga_dp:'',id:'',kondisi_barang:'',sku_barang:'',spesifikasi:''
  };
  nilaiCicilan;

  dataSimulasi; //Array loopin pilih simulasi cicilan
  typeCicilan; //Array loopin tampil
  simulasiCicilan; //Simulasi bulanan looping

  kalkulasi = {
    simulasi_cicilan: [], 
    harga_dp: 0, 
    total: 0,
    dp: 0,
    tenor:''
  };

  dataPrev;

  constructor(
    public navCtrl: NavController,
    public api: ApiServiceProvider, 
    public navParams: NavParams,
    public loadCtrl: LoadingController,
    public alertCtrl: AlertController,
    public storage: Storage) {
    this.detailProduk = 'detail';

    this.storage.get('dataPrev').then(data => {
      data = JSON.parse(data);
      this.dataPrev = data;
    })
  }
  ionViewWillEnter() {
        let id = this.navParams.get('idpersediaan');
        this.api.get('persediaan/detail?id='+id, produk => {
          let dataProduk    = produk.data
          this.imageSlider  = dataProduk.images;
          this.data         = produk.data
          this.typeCicilan  = dataProduk.type_cicilan
          this.dataSimulasi = dataProduk.simulasi_cicilan;
          this.kalkulasi.harga_dp = dataProduk.harga_dp
        }) //End Get Slider
  }

  loadNilaiCicilan(value) {
    //2 dp + 1 nodp
    this.kalkulasi.dp = value == 2 ? this.kalkulasi.harga_dp:0;
    let output = [];
    let simulasiCicilan = value == 2 ? this.dataSimulasi.dp:this.dataSimulasi.no_dp
    simulasiCicilan.forEach(item => {
      for(let key in item) {
        output.push({key: key, name: item[key]})
      }
    });
    this.simulasiCicilan = output
  }
  nilaiChange (value) {
    value = value.value.split('|')
    this.kalkulasi.simulasi_cicilan = [];

    let nilai_cicilan_bulan = value[1].replace(/Rp\./, '').replace(/\,/g, '');
    this.nilaiCicilan       = nilai_cicilan_bulan;

    let cicilan             = parseInt(value[0].replace('x', ''))
    this.kalkulasi.tenor    = value[0];

    for(let ke=1; ke <= cicilan; ke++) {
      this.kalkulasi.simulasi_cicilan.push({label: "Bulan "+ke, nilai: nilai_cicilan_bulan});
      this.kalkulasi.total = this.kalkulasi.total+parseInt(nilai_cicilan_bulan);
    }
  }

  tambahKeranjang() {
    let typeCicilan = this.kalkulasi.dp > 0 ? '2':'1';
    let form = new FormData;

    form.append('persediaan_id', this.data.id)
    form.append('nilai_cicilan_bulan', this.nilaiCicilan)
    form.append('type_cicilan', typeCicilan)
    form.append('cicilan_tenor', this.kalkulasi.tenor)
    form.append('dp', String(this.kalkulasi.dp))

    let loading = this.loadCtrl.create({
      content: "Mohon tunggu..."
    });

    loading.present();
    this.api.Post('pemesanan/cart/save', form).then((res: any) => {
      loading.dismiss()
      if(!res.status) {
          let alert = this.alertCtrl.create({
            title: 'Error',
            subTitle: res.message,
            buttons: ['OK']
          });
          alert.present()
      } else {
        if(this.dataPrev) {
          this.navCtrl.popToRoot()
          this.navCtrl.push(CartPage)
          this.navCtrl.push(KonsumenPage)
          this.navCtrl.push(DetailOrderPage)
        } else {
          this.navCtrl.popToRoot();
          this.navCtrl.push(CartPage);
        }
      }
    }).catch(err => [
      alert('Error')
    ])

  }

}
