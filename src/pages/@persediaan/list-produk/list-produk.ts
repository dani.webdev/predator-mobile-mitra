import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { ApiServiceProvider } from "../../../providers/api-service/api-service";
import { DetailProdukPage } from "../detail-produk/detail-produk";
/**
 * Generated class for the ListProdukPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-list-produk',
  templateUrl: 'list-produk.html',
})
export class ListProdukPage {

  listProduk = [{},{},{}]
  type;
  data;
  fillSearch; //Auto Fill Input Form

  constructor(
    public navCtrl: NavController,
    public api: ApiServiceProvider,
    public loadingCtrl: LoadingController,
    public navParams: NavParams) {

      this.listProduk = this.navParams.get('produk') ? this.navParams.get('produk'):[];
      this.fillSearch = this.navParams.get('fillSearch')

  }

  ionViewWillEnter() {
  }

  getDetail(id) {
      let loading = this.loadingCtrl.create({
        content: "Mohon tunggu..."
      });
      loading.present();
      this.navCtrl.push(DetailProdukPage, {
        idpersediaan: id
      })
      loading.dismiss()
  }

  onEnterSearch() {
    let query = this.fillSearch
    var loading = this.loadingCtrl.create({
      content: 'Memuat Produk...<br><small>"Orang sabar di sayang Tuhan :)"<small>'
    })
    loading.present()
    this.api.get('persediaan/filter?nama='+query, res => {
      loading.dismiss()
      if(res.data.total > 0) {
        this.listProduk = res.data.result
      } else {
        this.listProduk = []
      }
    })
  }
}
