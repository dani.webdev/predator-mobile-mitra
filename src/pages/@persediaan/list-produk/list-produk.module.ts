import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListProdukPage } from './list-produk';

@NgModule({
  declarations: [
    ListProdukPage,
  ],
  imports: [
    IonicPageModule.forChild(ListProdukPage),
  ],
})
export class ListProdukPageModule {}
