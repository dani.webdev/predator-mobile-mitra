import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetailOrderPage, ModalAjukan } from './detail-order';

@NgModule({
  declarations: [
    ModalAjukan,
    DetailOrderPage,
  ],
  entryComponents: [
    ModalAjukan,
  ],
  imports: [
    IonicPageModule.forChild(DetailOrderPage),
  ],
})
export class DetailOrderPageModule {}
