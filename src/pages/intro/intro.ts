import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { TabsPage } from '../tabs/tabs';
import { LoginPage } from '../login/login';

/**
 * Generated class for the IntroPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-intro',
  templateUrl: 'intro.html',
})
export class IntroPage {
  slides = [
    {
      title: "Welcome to the Docs!",
      description: "The <b>Ionic Component Documentation</b> showcases a number of useful components that are included out of the box with Ionic.",
      image: "assets/imgs/ica-slidebox-img-1.png",
    },
    {
      title: "What is Ionic?",
      description: "<b>Ionic Framework</b> is an open source SDK that enables developers to build high quality mobile apps with web technologies like HTML, CSS, and JavaScript.",
      image: "assets/imgs/ica-slidebox-img-2.png",
    },
    {
      title: "What is Ionic Cloud?",
      description: "The <b>Ionic Cloud</b> is a cloud platform for managing and scaling Ionic apps with integrated services like push notifications, native builds, user auth, and live updating.",
      image: "assets/imgs/ica-slidebox-img-3.png",
    }
  ]

  constructor(
    public navCtrl: NavController,
    private storage: Storage,
    private api: ApiServiceProvider,
    private loading: LoadingController,
    public navParams: NavParams) {
  }

  Skip() {

    let loading = this.loading.create({
      
      content: "Mohon Tunggu..."

    });
    loading.present();
    this.storage.get('token').then(token => {
      this.api.token = token;
      
      if(token) {
        
        let body = new FormData();
        body.append('token', token);
        this.api.Post('auth/check_token', body).then((data: any) => {
          
          loading.dismiss();
          if(data.status) {
            this.navCtrl.setRoot(TabsPage)
          } else {
            this.navCtrl.setRoot(LoginPage)
          }

        }).catch(err => {
          loading.dismiss();
          this.storage.remove('token')
          this.navCtrl.setRoot(LoginPage)          
        })

      } else {
        loading.dismiss();
        this.navCtrl.setRoot(LoginPage)          
      }
    })

  }

}
