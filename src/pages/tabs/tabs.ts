import { Component, ViewChild } from '@angular/core';
import { NavParams, Tabs, Tab } from "ionic-angular";

import { AccountPage } from '../account/account';
import { HomePage } from '../home/home';
import { BantuanPage } from "../bantuan/bantuan";
import { BeritaPage } from "../berita/berita";
import { TasPage } from "../@tas/tas/tas";

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  @ViewChild('Tabs') tabRef: Tabs

  DataKategori;

  tab1Root = HomePage
  tab2Root = BeritaPage
  tab3Root = TasPage
  tab4Root = BantuanPage
  tab5Root = AccountPage

  selected: number = 0;

  constructor(
    private params: NavParams
    ) {
      // this.tabRef.select(0)
    }
    ionViewDidEnter() {
      
    }
}
