import { Component } from '@angular/core';
import { NavController, AlertController, App } from 'ionic-angular';
import { Storage } from "@ionic/storage";
import { LoginPage } from "../login/login";
import { ApiServiceProvider } from "../../providers/api-service/api-service";

@Component({
  selector: 'page-account',
  templateUrl: 'account.html'
})

export class AccountPage {

  pengguna;
  avatar = '../../assets/imgs/avatar_cewe.png';
  cover  = '';

  constructor(
    public navCtrl: NavController,
    private alert: AlertController,
    private app: App,
    private api: ApiServiceProvider,
    private storage: Storage
    ) {
      this.api.Get('user/detail').then((res: any) => {
        this.pengguna = res.data[0];
      })

      this.api.Get('https://api.unsplash.com/search/photos?client_id=807ff42a73a31d6b845b4f80df033342eeb8ef15ce485ee54c8fb9626b41fa57&query=nature&per_page=1', true).then((res: any)=> {
        this.cover = res.results[0].urls.small
        console.log(this.cover)
      })
  }

  logout() {
    let alert = this.alert.create({
      title: "Apakah anda yakin..?",
      subTitle: "Keluar dari aplikasi Mitra Usaha",
      buttons: [
        {
          text: "Yakin",
          cssClass: 'btn-default',
          handler: () => {
            this.storage.clear()
            this.app.getRootNav().setRoot(LoginPage)
          }
        },
        {
          text: "Batal",
          handler: () => {
          }
        }
      ]
    })
    alert.present()
  }
}
