import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ApiServiceProvider } from '../../../providers/api-service/api-service';

/**
 * Generated class for the HotProspekPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-hot-prospek',
  templateUrl: 'hot-prospek.html',
})
export class HotProspekPage {


  result=[]
  originResult=[]
  noResult=false
  
  constructor(
    public navCtrl: NavController,
    private api: ApiServiceProvider,
    public navParams: NavParams) {
  }

  ionViewDidLoad() {
    this.api.Get('konsumen/prospek/nama?nama=&tgl').then((respon: any) => {
      this.result = respon.data.result;
      this.originResult = respon.data.result;
    })
  }

  goAddProspek() {

  }
  
  goDetail() {
    alert("Prospek Detail : Masih dalam tahap development.")
  }

  onInput(event) {
    let value   = event.srcElement.value ? event.srcElement.value:'';
    this.result = this.originResult;
    console.log(value);  
    let result  = this.result;
    let output  = [];
    result.forEach((item, index) => {
      let checkNama = item.nama_lengkap.toLowerCase().indexOf(value.toLowerCase())
      let checkPendamping = item.nama_pendamping ? item.nama_pendamping.toLowerCase().indexOf(value):-1
      if(checkNama != -1 || checkPendamping != -1) {
        output.push(item);
      }
    })
    this.noResult = false;
    if(!output.length) {
      this.noResult = true;
    }
    this.result = output
  }
}
