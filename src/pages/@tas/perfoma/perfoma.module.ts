import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PerfomaPage } from './perfoma';

@NgModule({
  declarations: [
    PerfomaPage,
  ],
  imports: [
    IonicPageModule.forChild(PerfomaPage),
  ],
})
export class PerfomaPageModule {}
