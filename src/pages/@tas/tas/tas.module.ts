import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TasPage } from './tas';
import { IonicImageLoader, ImgLoaderComponent } from "ionic-image-loader";

@NgModule({
  declarations: [
    TasPage,
  ],
  imports: [
    IonicImageLoader,
    IonicPageModule.forChild(TasPage),
  ],
  entryComponents: [
    ImgLoaderComponent
  ]
})
export class TasPageModule {}
