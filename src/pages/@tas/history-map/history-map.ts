import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ApiServiceProvider } from '../../../providers/api-service/api-service';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { DomSanitizer } from '@angular/platform-browser';


/**
 * Generated class for the HistoryMapPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-history-map',
  templateUrl: 'history-map.html',
})
export class HistoryMapPage {

  items;

  constructor(
    public navCtrl: NavController,
    private DomSanitizer: DomSanitizer,
    private api: ApiServiceProvider, 
    public navParams: NavParams) {
  }

  ionViewDidLoad() {
    this.api.Get('tas/history/data').then((res: any) => {
      this.items = res.status ? res.data.result: []
    })
  }

  onInput(value) {
    console.log(value)
  }

}
