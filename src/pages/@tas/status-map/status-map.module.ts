import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { StatusMapPage } from './status-map';

@NgModule({
  declarations: [
    StatusMapPage,
  ],
  imports: [
    IonicPageModule.forChild(StatusMapPage),
  ],
})
export class StatusMapPageModule {}
